document.getElementById('pay-button').addEventListener('click', function(event) {
    event.preventDefault(); // Prevent the default link behavior
    const payButton = document.getElementById('pay-button');
    const loadingGif = document.getElementById('loading-gif');

    // Disable the button
    payButton.disabled = true;

    // Show the loading GIF
    loadingGif.style.display = 'block';

    // Wait for 3 seconds and then redirect to the receipt page
    setTimeout(() => {
        window.location.href = 'receipt.html';
    }, 3000);
});
